# Mercedes Test QA

Hello, 

Thank you very much for the challenge.

I was very happy that it was very challenging.

Within the project, the first part of the challenge in "bug_report_mercedes.pages" is available. But I will send it by email too.

#### Extension to Visual Studio Code:

* [Robot Framework Intellisense](https://marketplace.visualstudio.com/items?itemName=TomiTurtiainen.rf-intellisense)

### Prerequisites
- Robot Framework uses the [Python](https://www.python.org/ftp/python/3.8.1/python-3.8.1.exe) for execution. So it is necessary to be installed on the machine
- Download the [ChromeDriver](https://chromedriver.chromium.org/downloads)
    * ChromeDriver Version must be compatible with your Chrome browser version (Ex: ChromeDriver 83.x.xxx.xx, Google Chrome Version 83.x.xxx.xx). To check the installed driver version, run: `chromedriver.exe --version`

#### Execute test And Install dependencies
    - Install all dependencies of the file requirements.txt

```
$ git clone https://gitlab.com/vinicios.borel/mercedes-test-qa.git
$ cd mercedes-test-qa
$ pip install -r requirements.txt
$ robot -d ./ReportResult  ./Tests/buy_car.robot
```

#### GitLab CI
    - The file .gitlab-ci.yml is responsible for run tests in pipeline, I created a Schedule to run every day at 06:00 am. 
    - To see results click in an pipeline and click in "Browser".
    - The file .txt with all models and price of cars is visible.
