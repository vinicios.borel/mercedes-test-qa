*** Settings ***
Resource    ../../env.robot

*** Keywords ***

Scrool to see "${text}"
    Scroll Page   0  600
    Wait Until Page Contains      ${text}        timeout=${WAIT_TIMEOUT}

Click to buy car
    select frame     ${mb-iframe}      
    Click Element                       ${plus-hb-aClass}
    Wait Until Element Is Visible         ${buy-car}        timeout=${WAIT_TIMEOUT}
    Click Element                       ${buy-car}

Filter by "${option_filter}" and Select "${option_fuel}"
    Set Suite Variable              ${FUEL}             ${option_fuel}
    Execute Javascript              ${filter-${option_filter}}
    Wait Until Page Contains        ${option_fuel}        timeout=${WAIT_TIMEOUT}
    Execute Javascript              ${${option_filter}-${option_fuel}}

User open filter in page of buy "${open_filter}"
    Wait Until Element Is Visible         ${filter-${open_filter}}        timeout=${WAIT_TIMEOUT}
    Click Element                   ${filter-${open_filter}}

Validate the Car Model in dropdown "${dropdown_value}"
    Wait Until Element Is Visible         ${dropdown_models}        timeout=${WAIT_TIMEOUT}
    ${value}=   Get Text            ${dropdown_models}
    Should Be Equal   ${dropdown_value}      ${value}

Validate the class selected "${model-car}"
    ${value}=   Get Text            ${title_model}
    Should Be Equal   ${model-car}      ${value}

Click in "${option}" submit filter
    Sleep  3
    Execute Javascript              ${filter-${option}}

Is displayed the result
    Wait Until Page Contains      New        timeout=${WAIT_TIMEOUT}
    Wait Until Page Contains      Used        timeout=${WAIT_TIMEOUT}
    Sleep  10    #I don't like of use to capture result but the site is very slow
    Capture Page Screenshot       ${TEST_NAME.replace(' ','_')}.png

Create file with name and price of all models
    Sleep  10
    ${script}=    getValue_name_price_all_models_displayed
    ${text}=  Execute Javascript   ${script}
    LOG  ${script}
    LOG   ${text}
    Create File    ${PATH} 
    Append To File  ./results/list_cars_price_${FUEL}.txt  ${text}