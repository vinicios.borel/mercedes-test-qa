*** Settings ***
Resource         ../env.robot
Test Teardown       Finish Test 

*** Test Cases ***


Buy Hatchbacks of type A-Class (Petrol)
    Given Open Mercedes-Benz UK
    When Click to buy car
    Then User is redirect to "shop.mercedes-benz.co.uk/"
        And Accept Terms
        And Is displayed the result

    Given User open filter in page of buy "Model"
        And Validate the Car Model in dropdown "Hatchback"
        And Validate the class selected "A-Class Hatchback"
        And Click in "Cancel" submit filter
    When Filter by "Fuel" and Select "Petrol"
        And Click in "Apply" submit filter
    Then Is displayed the result
        And Create file with name and price of all models


Buy Hatchbacks of type A-Class (Diesel)
    Given Open Mercedes-Benz UK
    When Click to buy car
    Then User is redirect to "shop.mercedes-benz.co.uk/"
        And Accept Terms
        And Is displayed the result

    Given User open filter in page of buy "Model"
        And Validate the Car Model in dropdown "Hatchback"
        And Validate the class selected "A-Class Hatchback"
        And Click in "Cancel" submit filter
    When Filter by "Fuel" and Select "Diesel"
        And Click in "Apply" submit filter
    Then Is displayed the result
        And Create file with name and price of all models


Buy Hatchbacks of type A-Class (Hybrid)
    Given Open Mercedes-Benz UK
    When Click to buy car
    Then User is redirect to "shop.mercedes-benz.co.uk/"
        And Accept Terms
        And Is displayed the result

    Given User open filter in page of buy "Model"
        And Validate the Car Model in dropdown "Hatchback"
        And Validate the class selected "A-Class Hatchback"
        And Click in "Cancel" submit filter
    When Filter by "Fuel" and Select "Hybrid"
        And Click in "Apply" submit filter
    Then Is displayed the result
        And Create file with name and price of all models


Buy Hatchbacks of type A-Class (Electric)
    Given Open Mercedes-Benz UK
    When Click to buy car
    Then User is redirect to "shop.mercedes-benz.co.uk/"
        And Accept Terms
        And Is displayed the result

    Given User open filter in page of buy "Model"
        And Validate the Car Model in dropdown "Hatchback"
        And Validate the class selected "A-Class Hatchback"
        And Click in "Cancel" submit filter
    When Filter by "Fuel" and Select "Electric"
        And Click in "Apply" submit filter
    Then Is displayed the result
        And Create file with name and price of all models





