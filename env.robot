*** Settings ***

##### Libraries #####
Library  SeleniumLibrary
Library  FakerLibrary    locale=pt_BR
Library  RequestsLibrary
Library  Collections
Library  Process
Library  OperatingSystem

Library  ./Commons/Scripts/getValue.py

##### Resources #####
Resource  ./Commons/PageObjects/objects.robot
Resource  ./Commons/commons_kw.robot
Resource  ./Tests/Pages/buy_car_kw.robot

*** Variables ***
${URL}                  http://www.mercedes-benz.co.uk
${BROWSER}              HeadlessChrome
${WAIT_TIMEOUT}         60
${PATH}                 ${CURDIR}/list_cars_price.txt
${WINDOW_WIDTH}             1920
${WINDOW_HEIGHT}            1080
