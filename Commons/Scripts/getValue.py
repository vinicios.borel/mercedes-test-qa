# Documentation:  user_manager_value_is_present_in_list
#
# Description:    Javascript query to verify is a given value is present in the user/role list
#
#
import sys 
sys.path.append('./Commons')

class getValue(object):
    def getValue_name_price_all_models_displayed(self):
        NAME_CAR_LIST = "('.vehicleItem__carInfo__header')"
        PRICE_CAR_LIST = "('.price--isBestOffer')"

        javascript_query = "var el = document.querySelectorAll('.vehicleItem__carInfo__header');" \
                "var pr = document.querySelectorAll('.price--isBestOffer');" \
                "var sl = document.querySelectorAll('.price--isSmall');" \
                "var fl = document.querySelectorAll('.vehicleItem__carInfo__spec div:nth-child(2) > span');"  \
                "var i = 0;" \
                "var objs = [];" \
                "while (i < el.length || i < pr.length  || i < fl.length || i < sl.length) {" + \
                "objs.push({" + \
                    "Model: el[i].innerText,"  \
                    "Price: {" + \
                            "Offer: pr[i].innerText," \
                            "Real: sl[i].innerText},"  \
                    "Fuel: fl[i].innerText" \
                "});" \
                "i++;" \
                "}; return JSON.stringify(objs);"
        return javascript_query


