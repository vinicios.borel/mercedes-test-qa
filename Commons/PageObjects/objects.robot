*** Settings ***
Resource    ../../env.robot  

*** Variable ***

#Page
${accept_all_terms}                 id=uc-btn-accept-banner
${mb-iframe}                        id=vmos-cont

#All Models
${model-Hatchbacks-iframe}          id=hatchbackid
${model-Hatchbacks}                 id=hatchback-id
${plus-hb-aClass}                   xpath=/html/body/div[1]/vmos/div/div/div/div/div/div[4]/section/div[1]/div[2]/div[1]/div/button
${buy-car}                          xpath=//a[contains(text(), "Buy online")]

#Filters
${filter-Model}                     css=.results > div  > div > nav > div > div:nth-child(1) > ul > li:nth-child(1) > button
${filter-Budget}                    document.querySelector(".results > div > div > nav > div > div:nth-child(1) > ul > li:nth-child(2) > button").click()
${filter-Fuel}                      document.querySelector(".results > div > div > nav > div > div:nth-child(1) > ul > li:nth-child(3) > button").click()
${filter-Gearbox}                   document.querySelector(".results > div > div > nav > div > div:nth-child(1) > ul > li:nth-child(4) > button").click()
${filter-Colour}                    document.querySelector(".results > div > div > nav > div > div:nth-child(1) > ul > li:nth-child(5) > button").click()
${filter-Location}                  document.querySelector(".results > div > div > nav > div > div:nth-child(1) > ul > li:nth-child(6) > button").click()
${filter-Search}                    document.querySelector(".results > div > div > nav > div > div:nth-child(1) > ul > li:nth-child(7) > button").click()

#Filter Model
${dropdown_models}                  class=dropdownItem__text
${title_model}                      class=modelItem__title
${filter-Cancel}                    document.querySelector(".results > div > div > div.filters__actions > div > button.button.filters__action__close.button--noBorder.button--uppercase").click()
${filter-Apply}                     document.querySelector(".results > div > div > div.filters__actions > div > button.button.button--secondary.button--uppercase").click()

#Options Fuel
${Fuel-Petrol}                      document.querySelector("#__layout > div > main > section > div > div > div.filters__filter-container > div > div > div > div > div > button:nth-child(1)").click()
${Fuel-Diesel}                      document.querySelector("#__layout > div > main > section > div > div > div.filters__filter-container > div > div > div > div > div > button:nth-child(2)").click()
${Fuel-Hybrid}                      document.querySelector("#__layout > div > main > section > div > div > div.filters__filter-container > div > div > div > div > div > button:nth-child(3)").click()
${Fuel-Electric}                    document.querySelector("#__layout > div > main > section > div > div > div.filters__filter-container > div > div > div > div > div > button:nth-child(4)").click()