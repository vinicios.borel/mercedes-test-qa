*** Settings ***
Resource    ../env.robot

*** Keywords ***

Open Mercedes-Benz UK
    Open Browser   ${URL}     ${BROWSER}    options=add_argument("--disable-dev-shm-usage"); add_argument("--no-sandbox")
    Delete All Cookies
    Title Should be           Mercedes-Benz Passenger Cars
    Accept Terms
    Sleep  5

Run Screenshots
    [Documentation]     The KW executes when the test finish and capture page screenshot with the name of the test
    Run Keyword And Ignore Error  Run keyword If Test Failed          Capture Page Screenshot     ${OUTPUTDIR}/Screenshots_${BROWSER}/Failed/${SUITE_NAME}/${TEST_NAME.replace(' ','_')}.png
    Run Keyword And Ignore Error  Run keyword If Test Passed          Capture Page Screenshot     ${OUTPUTDIR}/Screenshots_${BROWSER}/Success/${SUITE_NAME}/${TEST_NAME.replace(' ','_')}.png

Finish Test 
    Run Screenshots
    Close All Browsers

User is redirect to "${locale}"
    Wait Until Location Contains     ${locale}        timeout=${WAIT_TIMEOUT}
    Location Should Contain     ${locale}
    Set Window Size         ${WINDOW_WIDTH}         ${WINDOW_HEIGHT}

Sleep
    [Arguments]       ${time}
    BuiltIn.Sleep     ${time}     
    
Scroll Page
    [Arguments]     ${x}        ${y}
    Execute JavaScript      window.scrollTo(${x}, ${y})

Accept Terms
    Wait Until Element Is Visible      ${accept_all_terms}     timeout=${WAIT_TIMEOUT}
    Click Element           ${accept_all_terms} 

Click in "${options}"
    Wait Until Page Contains    ${options}               timeout=${WAIT_TIMEOUT}
    Click Element               xpath=//*[contains(text(), "${options}")]
